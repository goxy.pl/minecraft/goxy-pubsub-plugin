# Goxy PubSub

## Installation

### Requirements
- Java 8+
- Maven or Gradle

### Maven
```xml
<repositories>
    <repository>
        <id>goxy-maven</id>
        <url>https://repo.goxy.pl</url>
    </repository>
</repositories>

<dependencies>
    <dependency>
        <groupId>pl.goxy.minecraft</groupId>
        <artifactId>goxy-pubsub-api</artifactId>
        <version>0.0.7</version>
    </dependency>
</dependencies>
```

### Gradle
```groovy
repositories {
    maven {
        url "https://repo.goxy.pl"
    }
}

dependencies {
    compileOnly "pl.goxy.minecraft:goxy-pubsub-api:0.0.7"
}
```

## Usage
This usage show how to create plugin with hard dependency of pubsub, but pubsub also allows create plugin with soft dependency 

#### plugin.yml
```yaml
main: "pl.goxy.example.PubSubTestPlugin"
name: "PubSubTestPlugin"
version: "0.0.1"
author: "tirex"

depend:
  - "goxy-pubsub"
```

#### PubSubTestPlugin
```java
package pl.goxy.example;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;
import pl.goxy.minecraft.pubsub.PubSub;
import pl.goxy.minecraft.pubsub.PubSubService;

import java.util.Objects;

public class PubSubTestPlugin extends JavaPlugin implements Listener {
    private PubSub pubSub;

    @Override
    public void onEnable() {
        PluginManager pluginManager = this.getServer().getPluginManager();

        // Get pubsub plugin instance
        Plugin pubSubPlugin = Objects.requireNonNull(pluginManager.getPlugin("goxy-pubsub"), "goxy-pubsub");
        PubSubService pubSubService = (PubSubService) pubSubPlugin;
        this.pubSub = pubSubSerice.getPubSub(this);
        
        // Register pubsub handler on channel test with TestMessage as payload
        this.pubSub.registerHandler("test", TestMessage.class, (context, message) -> {
            this.getServer().broadcastMessage("<" + message.name + "> " + message.message);
        }).async(false);

        pluginManager.registerEvents(this, this);
    }

    @EventHandler
    public void onMessage(AsyncPlayerChatEvent event) {
        TestMessage message = new TestMessage();
        message.name = event.getPlayer().getName();
        message.message = event.getMessage();
        
        // Send pubsub payload on "test" channel in entire goxy network
        //   sendPluginNetwork - only current plugin will receive message on "test" channel
        //   sendNetwork - every plugin with registered "test" channel will receive message
        this.pubSub.sendPluginNetwork("test", message);
    }

    static class TestMessage {
        String name;
        String message;
    }
}
```