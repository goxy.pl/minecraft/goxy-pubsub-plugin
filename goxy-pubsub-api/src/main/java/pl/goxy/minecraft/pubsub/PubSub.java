package pl.goxy.minecraft.pubsub;

import pl.goxy.minecraft.api.network.GoxyContainer;
import pl.goxy.minecraft.api.network.GoxyServer;

import java.util.concurrent.CompletableFuture;

public interface PubSub
{
    CompletableFuture<Void> sendServer(String channel, Object payload, GoxyServer server);

    CompletableFuture<Void> sendPluginServer(String channel, Object payload, GoxyServer server);

    CompletableFuture<Void> sendContainer(String channel, Object payload, GoxyContainer container);

    CompletableFuture<Void> sendPluginContainer(String channel, Object payload, GoxyContainer container);

    CompletableFuture<Void> sendNetwork(String channel, Object payload);

    CompletableFuture<Void> sendPluginNetwork(String channel, Object payload);

    <T> PubSubHandlerOptions registerHandler(String channel, Class<T> dataType, PubSubDataHandler<T> handler);
}
