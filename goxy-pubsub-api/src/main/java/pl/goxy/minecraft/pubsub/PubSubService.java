package pl.goxy.minecraft.pubsub;

import org.bukkit.plugin.Plugin;

public interface PubSubService
{
    PubSub getPubSub(Plugin plugin);
}
