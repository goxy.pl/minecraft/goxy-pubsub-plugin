package pl.goxy.minecraft.pubsub;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

//todo add support to register method handlers with annotation
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface PubSubHandler
{
    String channel();

    boolean async() default false;

    boolean executeLocally() default true;
}
