package pl.goxy.minecraft.pubsub;

@FunctionalInterface
public interface PubSubDataHandler<T>
{
    void accept(PubSubContext context, T data);
}
