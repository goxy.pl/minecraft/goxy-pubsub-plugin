package pl.goxy.minecraft.pubsub;

public interface PubSubHandlerOptions
{
    boolean async();

    PubSubHandlerOptions async(boolean async);

    boolean executeLocally();

    PubSubHandlerOptions executeLocally(boolean executeLocally);
}
