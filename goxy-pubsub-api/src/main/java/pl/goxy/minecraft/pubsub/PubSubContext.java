package pl.goxy.minecraft.pubsub;

import pl.goxy.minecraft.api.network.GoxyServer;

public class PubSubContext
{
    private final GoxyServer server;
    private final boolean async;

    public PubSubContext(GoxyServer server, boolean async)
    {
        this.server = server;
        this.async = async;
    }

    public GoxyServer getServer()
    {
        return server;
    }

    public boolean isAsync()
    {
        return async;
    }
}
