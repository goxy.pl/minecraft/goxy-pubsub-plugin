package pl.goxy.minecraft.pubsub;

import org.bukkit.plugin.Plugin;
import pl.goxy.minecraft.api.network.GoxyContainer;
import pl.goxy.minecraft.api.network.GoxyServer;

import java.util.concurrent.CompletableFuture;

public class PubSubInstance
        implements PubSub
{
    private final Plugin plugin;
    private final PubSubHandlerService handlerService;

    public PubSubInstance(Plugin plugin, PubSubHandlerService handlerService)
    {
        this.plugin = plugin;
        this.handlerService = handlerService;
    }

    @Override
    public <T> PubSubHandlerOptions registerHandler(String channel, Class<T> dataType, PubSubDataHandler<T> handler)
    {
        return this.handlerService.registerHandler(this.plugin, channel, dataType, handler);
    }

    @Override
    public CompletableFuture<Void> sendServer(String channel, Object payload, GoxyServer server)
    {
        this.validChannel(channel);

        String redisChannel = "goxy.server." + server.getId();
        return this.handlerService.send(redisChannel, channel, payload);
    }

    @Override
    public CompletableFuture<Void> sendPluginServer(String channel, Object payload, GoxyServer server)
    {
        this.validChannel(channel);

        String redisChannel = "goxy.server." + server.getId();
        return this.handlerService.send(redisChannel, this.plugin.getName() + ":" + channel, payload);
    }

    @Override
    public CompletableFuture<Void> sendContainer(String channel, Object payload, GoxyContainer container)
    {
        this.validChannel(channel);

        String redisChannel = "goxy.container." + container.getId();
        return this.handlerService.send(redisChannel, channel, payload);
    }

    @Override
    public CompletableFuture<Void> sendPluginContainer(String channel, Object payload, GoxyContainer container)
    {
        this.validChannel(channel);

        String redisChannel = "goxy.container." + container.getId();
        return this.handlerService.send(redisChannel, this.plugin.getName() + ":" + channel, payload);
    }

    @Override
    public CompletableFuture<Void> sendNetwork(String channel, Object payload)
    {
        this.validChannel(channel);

        String redisChannel = "goxy.network";
        return this.handlerService.send(redisChannel, channel, payload);
    }

    @Override
    public CompletableFuture<Void> sendPluginNetwork(String channel, Object payload)
    {
        this.validChannel(channel);

        String redisChannel = "goxy.network";
        return this.handlerService.send(redisChannel, this.plugin.getName() + ":" + channel, payload);
    }

    private void validChannel(String channel)
    {
        if (channel.contains(" ") || channel.contains(":"))
        {
            throw new IllegalArgumentException("Invalid channel name");
        }
    }
}
