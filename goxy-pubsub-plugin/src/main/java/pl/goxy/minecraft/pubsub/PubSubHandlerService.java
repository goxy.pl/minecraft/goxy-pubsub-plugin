package pl.goxy.minecraft.pubsub;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.bukkit.plugin.Plugin;
import pl.goxy.minecraft.api.Goxy;
import pl.goxy.minecraft.api.network.GoxyServer;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionException;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

public class PubSubHandlerService
{
    private final Map<String, Handler<?>> handlerMap = new ConcurrentHashMap<>();

    private final Logger logger;
    private final Goxy goxy;
    private final JedisPool jedisPoolPublish;
    private final ObjectMapper objectMapper;

    public PubSubHandlerService(Logger logger, Goxy goxy, JedisPool jedisPoolPublish, ObjectMapper objectMapper)
    {
        this.logger = logger;
        this.goxy = goxy;
        this.jedisPoolPublish = jedisPoolPublish;
        this.objectMapper = objectMapper;
    }

    public <T> PubSubHandlerOptions registerHandler(Plugin plugin, String channel, Class<T> dataType,
            PubSubDataHandler<T> handler)
    {
        String pluginChannelName = plugin.getName() + ":" + channel;
        if (this.handlerMap.containsKey(pluginChannelName))
        {
            throw new IllegalStateException(
                    "Handler " + channel + " for plugin " + plugin.getName() + " is already defined!");
        }
        Handler<T> internalHandler = new Handler<>();
        internalHandler.plugin = plugin;
        internalHandler.channel = channel;
        internalHandler.handler = handler;
        internalHandler.type = dataType;
        this.handlerMap.put(pluginChannelName, internalHandler);
        this.handlerMap.put(channel, internalHandler);

        this.logger.info("Register handler " + channel + " for plugin " + plugin.getName());

        HandlerOptions handlerOptions = new HandlerOptions();
        handlerOptions.handler = internalHandler;
        return handlerOptions;
    }

    public CompletableFuture<Void> send(String redisChannel, String channel, Object payload)
    {
        return CompletableFuture.runAsync(() ->
        {
            try
            {
                String message = this.objectMapper.writeValueAsString(payload);
                GoxyServer server = this.goxy.getNetworkManager().getServer();
                String serverId = server != null ? server.getId().toString() : "";
                String redisMessage = channel + PubSubListener.ARG_SEPARATOR + serverId + PubSubListener.ARG_SEPARATOR + message;
                if (server != null)
                {
                    Handler<Object> handler = this.getHandler(channel);
                    if (handler != null && handler.executeLocally)
                    {
                        this.handle(handler, server, payload);
                    }
                }
                try (Jedis jedis = this.jedisPoolPublish.getResource())
                {
                    jedis.publish(redisChannel, redisMessage);
                }
            }
            catch (JsonProcessingException e)
            {
                throw new CompletionException(e);
            }
        });
    }

    public Handler<Object> getHandler(String channel)
    {
        return (Handler<Object>) this.handlerMap.get(channel);
    }

    public void handle(Handler<Object> handler, GoxyServer server, Object data)
    {
        PubSubContext context = new PubSubContext(server, handler.async);
        if (context.isAsync())
        {
            handler.handler.accept(context, data);
            return;
        }
        handler.plugin.getServer().getScheduler().runTask(handler.plugin, () ->
        {
            handler.handler.accept(context, data);
        });
    }

    public void handle(String channel, UUID serverId, String message)
    {
        GoxyServer currentServer = this.goxy.getNetworkManager().getServer();
        if (currentServer != null && currentServer.getId().equals(serverId))
        {
            return;
        }
        Handler<Object> handler = this.getHandler(channel);
        if (handler == null)
        {
            this.logger.warning("Can not find handler " + channel);
            return;
        }
        GoxyServer server = serverId != null ? this.goxy.getNetworkManager().getServer(serverId) : null;
        try
        {
            Object data = this.objectMapper.readValue(message, handler.type);
            this.handle(handler, server, data);
        }
        catch (JsonProcessingException e)
        {
            this.logger.log(Level.SEVERE, "Receive invalid json payload", e);
        }
    }

    static class Handler<T>
    {
        Plugin plugin;
        String channel;
        PubSubDataHandler<T> handler;
        Class<T> type;
        boolean async;
        boolean executeLocally = true;
    }

    static class HandlerOptions
            implements PubSubHandlerOptions
    {
        Handler<?> handler;

        @Override
        public boolean async()
        {
            return handler.async;
        }

        @Override
        public PubSubHandlerOptions async(boolean async)
        {
            handler.async = async;
            return this;
        }

        @Override
        public boolean executeLocally()
        {
            return handler.executeLocally;
        }

        @Override
        public PubSubHandlerOptions executeLocally(boolean executeLocally)
        {
            handler.executeLocally = executeLocally;
            return this;
        }
    }
}
