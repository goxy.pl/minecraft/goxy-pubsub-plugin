package pl.goxy.minecraft.pubsub;

import redis.clients.jedis.JedisPubSub;

import java.util.UUID;
import java.util.logging.Logger;

public class PubSubJedisListener
        extends JedisPubSub
        implements PubSubListener
{
    private final Logger logger;

    private final PubSubHandlerService handlerService;

    public PubSubJedisListener(Logger logger, PubSubHandlerService handlerService)
    {
        this.logger = logger;
        this.handlerService = handlerService;
    }

    @Override
    public void onMessage(String redisChannel, String redisMessage)
    {
        String[] strings = redisMessage.split(ARG_SEPARATOR);
        if (strings.length < 2)
        {
            throw new IllegalStateException("Invalid redis message");
        }
        String channel = strings[0];
        UUID serverId = strings.length > 2 && !strings[1].isEmpty() ? UUID.fromString(strings[1]) : null;
        String message = strings[strings.length - 1];

        this.handlerService.handle(channel, serverId, message);
    }

    @Override
    public void onSubscribe(String channel, int subscribedChannels)
    {
        this.logger.info("Subscribe redis channel with name '" + channel + "'");
    }

    @Override
    public void onUnsubscribe(String channel, int subscribedChannels)
    {
        this.logger.info("Unsubscribe redis channel with name '" + channel + "'");
    }
}
