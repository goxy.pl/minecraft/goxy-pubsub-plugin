package pl.goxy.minecraft.pubsub;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import pl.goxy.minecraft.api.Goxy;
import pl.goxy.minecraft.api.event.GoxyContainerChangedEvent;
import pl.goxy.minecraft.api.event.GoxyInitializedEvent;
import pl.goxy.minecraft.api.network.GoxyContainer;
import pl.goxy.minecraft.api.network.GoxyServer;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.logging.Level;

public class PubSubPlugin
        extends JavaPlugin
        implements PubSubService, Listener
{
    private Jedis jedis;
    private JedisPool jedisPoolPublish;

    private PubSubHandlerService handlerService;
    private PubSubJedisListener pubSubListener;

    @Override
    public void onEnable()
    {
        this.saveDefaultConfig();

        FileConfiguration configuration = this.getConfig();
        String redisUrl = configuration.getString("redis.url");
        if (redisUrl == null)
        {
            this.getLogger().warning("You need to set redis.url configuration field");
            this.getPluginLoader().disablePlugin(this);
            return;
        }
        this.jedis = new Jedis(redisUrl);
        this.jedisPoolPublish = new JedisPool(redisUrl);

        PluginManager pluginManager = this.getServer().getPluginManager();
        Goxy goxy = (Goxy) Objects.requireNonNull(pluginManager.getPlugin("goxy"), "goxy");

        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
        objectMapper.registerModule(new JavaTimeModule());

        this.handlerService = new PubSubHandlerService(this.getLogger(), goxy, this.jedisPoolPublish, objectMapper);

        this.pubSubListener = new PubSubJedisListener(this.getLogger(), this.handlerService);
        Thread thread = new Thread(() ->
        {
            try
            {
                this.getLogger().info("Subscribe to redis");
                Set<String> channels = new HashSet<>();
                channels.add("goxy.network");
                if (goxy.getNetworkManager().isInitialized())
                {
                    GoxyServer server = Objects.requireNonNull(goxy.getNetworkManager().getServer(), "server");
                    GoxyContainer container = Objects.requireNonNull(goxy.getNetworkManager().getContainer(),
                            "container");

                    channels.add("goxy.server." + server.getId());
                    channels.add("goxy.container." + container.getId());
                }
                try (Jedis jedis = this.jedis)
                {
                    jedis.subscribe(this.pubSubListener, channels.toArray(new String[0]));
                }
                this.getLogger().info("After subscribe to redis");
            }
            catch (Throwable throwable)
            {
                this.getLogger().log(Level.SEVERE, "An error occurred while subscribe to jedis pubsub", throwable);
            }
        });
        thread.setDaemon(false);
        thread.start();

        pluginManager.registerEvents(this, this);
    }

    @Override
    public void onDisable()
    {
        if (this.pubSubListener != null)
        {
            this.pubSubListener.unsubscribe();
        }
        if (this.jedisPoolPublish != null)
        {
            this.jedisPoolPublish.close();
        }
    }

    @EventHandler
    public void initializeGoxy(GoxyInitializedEvent event)
    {
        if (!this.pubSubListener.isSubscribed())
        {
            return;
        }
        Goxy goxy = event.getGoxy();

        GoxyServer server = Objects.requireNonNull(goxy.getNetworkManager().getServer(), "server");
        GoxyContainer container = Objects.requireNonNull(goxy.getNetworkManager().getContainer(), "container");

        this.pubSubListener.subscribe("goxy.server." + server.getId());
        this.pubSubListener.subscribe("goxy.container." + container.getId());
    }

    @EventHandler
    public void changeContainer(GoxyContainerChangedEvent event)
    {
        if (!this.pubSubListener.isSubscribed())
        {
            return;
        }
        this.pubSubListener.unsubscribe("goxy.container." + event.getOldContainer().getId());

        Goxy goxy = event.getGoxy();

        GoxyContainer container = Objects.requireNonNull(goxy.getNetworkManager().getContainer(), "container");
        this.pubSubListener.subscribe("goxy.container." + container.getId());
    }

    @Override
    public PubSub getPubSub(Plugin plugin)
    {
        return new PubSubInstance(plugin, this.handlerService);
    }
}
